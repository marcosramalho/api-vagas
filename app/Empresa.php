<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        "nomeFantasia", "cnpj", "email"
    ];
    

    // mapeamento entre empresa e vaga -- 1 empresa pode ter várias vagas
    public function vagas() {
        return $this->hasMany("App\Vaga", 'empresa_id');
    }
}
